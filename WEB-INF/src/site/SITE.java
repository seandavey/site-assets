package site;

import app.EditSite;
import app.HomePage;
import app.People;
import app.Request;
import app.Site;
import db.DBConnection;
import sitemenu.SiteMenu;
import web.Head;

public class SITE extends Site {
	public
	SITE(String base_directory) {
		super(base_directory, "Home", "site");
	}

	//--------------------------------------------------------------------------

	@Override
	public void
	init(DBConnection db) {
		addModule(new EditSite(), db);
		addModule(new HomePage(), db);
		addModule(new People(), db);
		addModule(new SiteMenu(), db);
	}

	//--------------------------------------------------------------------------

	@Override
	public void
	pageOpen(String current_page, Head head, boolean open_main, Request r) {
		if (head != null)
			head.styleSheet("flatpickr.min", false).close();
		SiteMenu site_menu = (SiteMenu)getModule("SiteMenu");
		if (site_menu != null) {
			site_menu.write(current_page, r);
			r.w.setId("page").addStyle("flex-grow:1").tagOpen("div");
		}
		if (open_main)
			r.w.mainOpen();
	}
}
